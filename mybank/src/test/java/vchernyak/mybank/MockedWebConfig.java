package vchernyak.mybank;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.rules.service.impl.LoanApplicationService;
import vchernyak.mybank.rules.service.impl.LoanExtensionService;
import vchernyak.mybank.web.controller.impl.LoansController;
import vchernyak.mybank.web.service.LoansControllerService;

import static org.mockito.Mockito.mock;

@Configuration
@EnableWebMvc
public class MockedWebConfig {

    @Bean
    public UserRepository userRepository() {
        return mock(UserRepository.class);
    }

    @Bean
    public LoanRepository loanRepository() {
        return mock(LoanRepository.class);
    }

    @Bean
    public LoanApplicationService loanApplicationService() {
        return mock(LoanApplicationService.class);
    }

    @Bean
    public LoanExtensionService loanExtensionService() {
        return mock(LoanExtensionService.class);
    }

    @Bean
    public LoansControllerService loansControllerService() {
        return new LoansControllerService();
    }

    @Bean
    public LoansController loansController() {
        return new LoansController();
    }

}
