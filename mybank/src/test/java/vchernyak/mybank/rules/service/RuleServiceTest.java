package vchernyak.mybank.rules.service;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.drools.core.command.runtime.rule.FireAllRulesCommand;
import org.drools.core.command.runtime.rule.InsertObjectCommand;
import org.drools.core.command.runtime.rule.QueryCommand;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.rules.data.fact.NamedFact;
import vchernyak.mybank.rules.data.input.RuleInput;
import vchernyak.mybank.rules.data.result.RuleResult;
import vchernyak.mybank.rules.log.EventListenerLogger;

import java.util.*;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class RuleServiceTest {

    private final StatelessKieSession kieSession = mock(StatelessKieSession.class);

    private final EventListenerLogger logger = mock(EventListenerLogger.class);

    private final KieBase kieBase = mock(KieBase.class);

    private final LoanRepository loanRepository = mock(LoanRepository.class);

    private RulesService<Object, RuleInput<Object>> rulesService;

    @Before
    public void before() {
        rulesService = spy(new RulesService<Object, RuleInput<Object>>(logger, loanRepository) {
            @Override
            protected KieBase kieBase() {
                return kieBase;
            }
        });
    }

    @After
    public void after() {
        reset(kieSession, logger, kieBase, loanRepository);
    }

    @Test
    public void newSession() {
        when(kieBase.newStatelessKieSession()).thenReturn(kieSession);

        assertEquals(kieSession, rulesService.newSession());
        verify(kieSession).addEventListener(logger);
    }

    @Test
    public void prepareCommands() {
        NamedFact fact1 = newNamedFact("fact1", "value1");
        NamedFact fact2 = newNamedFact("fact2", "value2");
        RuleInput<Object> input = newRuleInput(Arrays.asList(fact1, fact2), "query");
        List<Command> commands = rulesService.prepareCommands(input);

        assertEquals(6, commands.size());

        assertThat(commands.get(0), instanceOf(InsertObjectCommand.class));
        assertThat(commands.get(1), instanceOf(InsertObjectCommand.class));
        assertThat(commands.get(2), instanceOf(FireAllRulesCommand.class));
        assertThat(commands.get(3), instanceOf(QueryCommand.class));
        assertThat(commands.get(4), instanceOf(QueryCommand.class));
        assertThat(commands.get(5), instanceOf(QueryCommand.class));

        assertEquals(fact1, ((InsertObjectCommand) commands.get(0)).getObject());
        assertEquals(fact2, ((InsertObjectCommand) commands.get(1)).getObject());
        assertEquals("session.fireAllRules();", commands.get(2).toString());
        assertEquals(RulesService.QUERY_SUCCESS, ((QueryCommand)commands.get(3)).getName());
        assertEquals(RulesService.QUERY_MESSAGES, ((QueryCommand)commands.get(4)).getName());
        assertEquals( "query", ((QueryCommand)commands.get(5)).getName());
    }

    @Test
    public void handleResultSuccess() {
        RuleInput<Object> ruleInput = mock(RuleInput.class);
        when(ruleInput.resultQuery()).thenReturn("query");

        QueryResultsRow success = mock(QueryResultsRow.class);
        when(success.get(RulesService.QUERY_SUCCESS)).thenReturn(true);

        QueryResultsRow result = mock(QueryResultsRow.class);
        when(result.get(ruleInput.resultQuery())).thenReturn("result");

        ExecutionResults execResults = mock(ExecutionResults.class);
        when(execResults.getValue(RulesService.QUERY_SUCCESS)).thenReturn(newQueryResults(success));
        when(execResults.getValue(RulesService.QUERY_MESSAGES)).thenReturn(newQueryResults());
        when(execResults.getValue(ruleInput.resultQuery())).thenReturn(newQueryResults(result));

        RuleResult<Object> ruleResult = rulesService.handleResult(execResults, ruleInput);
        RuleResult<String> expectedResult = new RuleResult<>(true, Collections.EMPTY_LIST, "result");

        assertEquals(expectedResult, ruleResult);
    }

    @Test
    public void handleResultFail() {
        RuleInput<Object> ruleInput = mock(RuleInput.class);
        when(ruleInput.resultQuery()).thenReturn("query");

        QueryResultsRow success = mock(QueryResultsRow.class);
        when(success.get(RulesService.QUERY_SUCCESS)).thenReturn(false);

        QueryResultsRow message1 = mock(QueryResultsRow.class);
        when(message1.get(RulesService.QUERY_MESSAGES)).thenReturn("message1");
        QueryResultsRow message2 = mock(QueryResultsRow.class);
        when(message2.get(RulesService.QUERY_MESSAGES)).thenReturn("message2");

        ExecutionResults execResults = mock(ExecutionResults.class);
        when(execResults.getValue(RulesService.QUERY_SUCCESS)).thenReturn(newQueryResults(success));
        when(execResults.getValue(RulesService.QUERY_MESSAGES)).thenReturn(newQueryResults(message1, message2));
        when(execResults.getValue(ruleInput.resultQuery())).thenReturn(newQueryResults());

        RuleResult<Object> ruleResult = rulesService.handleResult(execResults, ruleInput);
        RuleResult<Object> expectedResult = new RuleResult<>(false, Arrays.asList("message1", "message2"), null);

        assertEquals(expectedResult, ruleResult);
    }

    @Test
    public void process() {
        ExecutionResults executionResults = mock(ExecutionResults.class);
        doReturn(executionResults).when(kieSession).execute(any(Command.class));

        RuleInput<Object> ruleInput = mock(RuleInput.class);
        when(ruleInput.facts()).thenReturn(new ArrayList<>());
        when(ruleInput.resultQuery()).thenReturn("result");

        when(kieBase.newStatelessKieSession()).thenReturn(kieSession);

        doReturn(new ArrayList<Command>()).when(rulesService).prepareCommands(ruleInput);
        doReturn(kieSession).when(rulesService).newSession();
        doReturn(mock(RuleResult.class)).when(rulesService).handleResult(any(ExecutionResults.class), any(RuleInput.class));

        rulesService.process(ruleInput);

        verify(rulesService).prepareCommands(ruleInput);
        verify(kieSession).setGlobal(RulesService.GLOBAL_LOAN_REPOSITORY, loanRepository);
        verify(rulesService).handleResult(executionResults, ruleInput);
    }

    private QueryResults newQueryResults(QueryResultsRow ...rows) {
        final List<QueryResultsRow> list = Arrays.asList(rows);

        return new QueryResults() {
            @Override
            public String[] getIdentifiers() {
                return new String[0];
            }

            @Override
            public Iterator<QueryResultsRow> iterator() {
                return list.iterator();
            }

            @Override
            public int size() {
                return list.size();
            }
        };
    }


    private NamedFact newNamedFact(final String name, final String value) {
        return new NamedFact(name) {

            public Object getValue() {
                return value;
            }

            @Override
            public boolean equals(Object o) {
                return EqualsBuilder.reflectionEquals(this, o);
            }

            @Override
            public int hashCode() {
                return HashCodeBuilder.reflectionHashCode(this);
            }
        };
    }

    private RuleInput<Object> newRuleInput(final List<NamedFact> facts, final String query) {
        return new RuleInput<Object>() {
            @Override
            public List<NamedFact> facts() {
                return facts;
            }

            @Override
            public String resultQuery() {
                return query;
            }
        };
    }

}
