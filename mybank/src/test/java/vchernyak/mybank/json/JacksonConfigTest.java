package vchernyak.mybank.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import vchernyak.mybank.PropertiesConfig;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.dao.entity.UserActionInfo;
import vchernyak.mybank.security.auth.SecurityUser;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PropertiesConfig.class, JacksonConfig.class})
public class JacksonConfigTest {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    @Qualifier("jsonMapper")
    private ObjectMapper mapper;

    @Test
    public void mapperDateTime() throws IOException {
        DateTime now = DateTime.now(DateTimeZone.UTC).withMillis(0);
        String serialized = mapper.writeValueAsString(now);
        assertEquals("\"" + now.toString(DATE_FORMAT) + "\"" , serialized);
        assertEquals(now, mapper.readValue(serialized, DateTime.class));
    }

    @Test
    public void mapperUserNamePasswordAuthenticationToken() throws IOException {
        String login = "{\"username\" : \"user\", \"password\" : \"password\"}";
        UsernamePasswordAuthenticationToken token = mapper.readValue(login, UsernamePasswordAuthenticationToken.class);
        assertEquals("user", token.getPrincipal());
        assertEquals("password", token.getCredentials());
    }

    @Test
    public void mapperSecurityUser() throws IOException {
        DateTime now = DateTime.now(DateTimeZone.UTC).withMillis(0);
        SecurityUser securityUser = new SecurityUser("user", "password", now);
        String serialized = mapper.writeValueAsString(securityUser);
        assertEquals("{\"username\":\"user\",\"expirationTime\":\"" + now.toString(DATE_FORMAT) + "\"}", serialized);

        securityUser = new SecurityUser("user", null, now);
        assertEquals(securityUser, mapper.readValue(serialized, SecurityUser.class));
    }

    @Test
    public void mapperLoan() throws JsonProcessingException {
        DateTime now = DateTime.now(DateTimeZone.UTC);
        Loan tiedLoan = new Loan.Builder().build();
        tiedLoan.setId(1l);

        Loan loan = new Loan.Builder()
                .withUser(mock(User.class))
                .withAmount(1)
                .withEnd(now)
                .withStart(now)
                .withInterest(1)
                .withExtended(tiedLoan)
                .withUserActionInfo(mock(UserActionInfo.class))
        .build();
        loan.setExtension(tiedLoan);

        assertEquals("{\"id\":null,\"amount\":1.0,\"interest\":1.0,\"start\":\"" + now.toString(DATE_FORMAT) + "\",\"end\":\"" + now.toString(DATE_FORMAT) + "\",\"closed\":false,\"extendedId\":1,\"extensionId\":1}", mapper.writeValueAsString(loan));
    }

}
