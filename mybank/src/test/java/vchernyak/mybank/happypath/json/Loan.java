package vchernyak.mybank.happypath.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Loan {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("interest")
    private Double interest;

    @JsonProperty("start")
    private DateTime start;

    @JsonProperty("end")
    private DateTime end;

    @JsonProperty("closed")
    private Boolean closed;

    @JsonProperty("extendedId")
    private Integer extendedId;

    @JsonProperty("extensionId")
    private Integer extensionId;

    public Long getId() {
        return id;
    }

    public Double getAmount() {
        return amount;
    }

    public Double getInterest() {
        return interest;
    }

    public DateTime getStart() {
        return start;
    }

    public DateTime getEnd() {
        return end;
    }

    public Boolean getClosed() {
        return closed;
    }

    public Integer getExtendedId() {
        return extendedId;
    }

    public Integer getExtensionId() {
        return extensionId;
    }
}
