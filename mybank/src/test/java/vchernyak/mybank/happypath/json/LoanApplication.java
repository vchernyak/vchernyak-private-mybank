package vchernyak.mybank.happypath.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanApplication {

    @JsonProperty("amount")
    private final Double amount;

    @JsonProperty("end")
    private final DateTime end;

    public LoanApplication(Double amount, DateTime end) {
        this.amount = amount;
        this.end = end;
    }

}
