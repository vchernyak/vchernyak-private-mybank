package vchernyak.mybank.happypath.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginError {

    @JsonProperty("reason")
    private String reason;

    @JsonProperty("message")
    private String message;

    public String getReason() {
        return reason;
    }

    public String getMessage() {
        return message;
    }
}
