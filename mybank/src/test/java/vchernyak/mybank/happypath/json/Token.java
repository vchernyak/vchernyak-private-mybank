package vchernyak.mybank.happypath.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Token {

    @JsonProperty("token")
    private String token;

    @JsonProperty("expirationTime")
    private DateTime expirationTime;

    public String getToken() {
        return token;
    }

    public DateTime getExpirationTime() {
        return expirationTime;
    }
}
