package vchernyak.mybank.happypath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import vchernyak.mybank.ApplicationConfig;
import vchernyak.mybank.HappyPathConfig;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.happypath.util.Client;
import vchernyak.mybank.happypath.util.HappyTester;
import vchernyak.mybank.happypath.util.HappyTesterResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ApplicationConfig.class, HappyPathConfig.class})
@WebIntegrationTest
public class HappyPathTest {

    private static final int USERS_COUNT = 2000;

    private static final int THREAD_POOL_SIZE = 500;

    private final Logger log = LoggerFactory.getLogger(HappyPathTest.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Client client;

    @Test
    public void happy() throws InterruptedException, ExecutionException {
        List<User> users = new ArrayList<>();
        List<HappyTester> testers = new ArrayList<>();
        for (int i = 0; i < USERS_COUNT; i++) {
            User user = new User.Builder()
                    .withUsername("user" + i)
                    .withPassword(passwordEncoder.encode("user" + i))
                    .build();

            users.add(user);
            testers.add(new HappyTester(user, client));
        }
        userRepository.save(users);

        ExecutorService executor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        List<Future<HappyTesterResult>> results = executor.invokeAll(testers);

        boolean everyUserPassed = true;
        for (Future<HappyTesterResult> resultFuture : results) {
            HappyTesterResult result = resultFuture.get();
            if(!result.isSuccess()) {
                everyUserPassed = false;
                log.error("Happy path test failed for user {}", result.getUsername());
                if(result.getException() != null) {
                    log.error("Exception for user is", result.getException());
                }
            } else {
                List<Loan> userLoans = loanRepository.findByUserOrderByEndAsc(userRepository.findByUsername(result.getUsername()));
                if(userLoans.size() != 2) {
                    everyUserPassed = false;
                    log.error("Happy path test failed for user {}. {} loans created instead of 2", result.getUsername(), userLoans.size());
                } else {
                    if(!userLoans.get(0).isClosed() || userLoans.get(0).getExtension() == null) {
                        everyUserPassed = false;
                        log.error("Happy path test failed for user {}. Improper loan extension.", result.getUsername());
                    }
                }
            }
        }

        assertTrue(everyUserPassed);
    }

}
