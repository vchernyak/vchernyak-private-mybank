package vchernyak.mybank.happypath.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.happypath.json.Token;

import java.util.concurrent.Callable;

public class HappyTester implements Callable<HappyTesterResult> {

    private final User user;

    private final Client client;

    public HappyTester(User user, Client client) {
        this.user = user;
        this.client = client;
    }

    @Override
    public HappyTesterResult call() throws Exception {
        try {
            Token token = client.login(user.getUsername(), user.getUsername());
            String loanLocation =client.applyLoan(token.getToken(), 10d, DateTime.now(DateTimeZone.UTC).plusMonths(1));
            client.extendLoan(token.getToken(), loanLocation);

            return new HappyTesterResult(true, user.getUsername());
        } catch (Exception e) {
            return new HappyTesterResult(false, user.getUsername(), e);
        }
    }
}
