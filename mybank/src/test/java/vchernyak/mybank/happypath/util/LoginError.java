package vchernyak.mybank.happypath.util;

public class LoginError extends Exception {

    private final LoginError details;

    public LoginError(LoginError details) {
        this.details = details;
    }

    public LoginError getDetails() {
        return details;
    }
}
