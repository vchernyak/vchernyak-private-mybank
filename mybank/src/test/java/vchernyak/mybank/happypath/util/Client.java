package vchernyak.mybank.happypath.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import vchernyak.mybank.happypath.json.LoanApplication;
import vchernyak.mybank.happypath.json.Login;
import vchernyak.mybank.happypath.json.Token;

import java.io.IOException;
import java.util.List;

@Component
public class Client {

    private static final String BASE_URL = "http://localhost:%s";
    private static final String LOGIN_URL = "/login";
    private static final String LOANS_URL = "/loans";
    private static final String EXTEND_URL = "%s/extend";

    private final String port;

    private final String authTokenHeaderName;

    private final ObjectMapper restObjectMapper;

    private final RestTemplate restTemplate;

    @Autowired
    public Client(RestTemplate restTemplate, @Qualifier("restObjectMapper") ObjectMapper restObjectMapper,
                  @Value("${server.port}") String port, @Value("${token.header}") String authTokenHeaderName) {
        this.restTemplate = restTemplate;
        this.restObjectMapper = restObjectMapper;
        this.port = port;
        this.authTokenHeaderName = authTokenHeaderName;
    }

    public Token login(String username, String password) throws LoginError, IOException {
        Login login = new Login(username, password);
        ResponseEntity<String> response = restTemplate.exchange(loginUrl(), HttpMethod.POST, entity(login), String.class);

        if(response.getStatusCode() == HttpStatus.OK) {
            return restObjectMapper.readValue(response.getBody(), Token.class);
        } else {
            throw new LoginError(restObjectMapper.readValue(response.getBody(), LoginError.class));
        }
    }

    public String applyLoan(String token, double amount, DateTime endDate) throws IOException, ApiError {
        LoanApplication loanApplication = new LoanApplication(amount, endDate);

        ResponseEntity<String> response = restTemplate.exchange(loansUrl(), HttpMethod.POST, entity(loanApplication, token), String.class);
        if(response.getStatusCode() == HttpStatus.CREATED) {
            return response.getHeaders().getFirst("Location");
        } else {
            throw new ApiError(restObjectMapper.readValue(response.getBody(), new TypeReference<List<String>>() {}));
        }
    }

    public String extendLoan(String token, String location) throws IOException, ApiError {
        ResponseEntity<String> response = restTemplate.exchange(extensionUrl(location), HttpMethod.POST, entity(null, token), String.class);
        if(response.getStatusCode() == HttpStatus.CREATED) {
            return response.getHeaders().getFirst("Location");
        } else {
            throw new ApiError(restObjectMapper.readValue(response.getBody(), new TypeReference<List<String>>() {}));
        }
    }

    private <T> HttpEntity<T> entity(T body) {
        return new HttpEntity(body);
    }

    private <T> HttpEntity<T> entity(T body, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(authTokenHeaderName, token);

        return new HttpEntity(body, headers);
    }

    private String baseUrl() {
        return String.format(BASE_URL, port);
    }

    private String loginUrl() {
        return baseUrl() + LOGIN_URL;
    }

    private String loansUrl() {
        return baseUrl() + LOANS_URL;
    }

    private String extensionUrl(String location) {
        return String.format(baseUrl() + EXTEND_URL, location);
    }

}
