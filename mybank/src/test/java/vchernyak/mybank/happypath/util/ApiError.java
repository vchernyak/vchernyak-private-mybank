package vchernyak.mybank.happypath.util;

import java.util.List;

public class ApiError extends Exception {

    private final List<String> messages;

    public ApiError(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getMessages() {
        return messages;
    }
}
