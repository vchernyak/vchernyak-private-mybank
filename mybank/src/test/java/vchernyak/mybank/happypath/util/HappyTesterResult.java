package vchernyak.mybank.happypath.util;

public class HappyTesterResult {

    public HappyTesterResult(boolean success, String username, Exception exception) {
        this.username = username;
        this.success = success;
        this.exception = exception;
    }

    public HappyTesterResult(boolean success, String username) {
        this.username = username;
        this.success = success;
    }

    private String username;

    private boolean success;

    private Exception exception;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
