package vchernyak.mybank;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import vchernyak.mybank.happypath.json.DateTimeDeserializer;
import vchernyak.mybank.happypath.json.DateTimeSerializer;

@Configuration
public class HappyPathConfig {

    @Autowired
    @Qualifier("restObjectMapper")
    private ObjectMapper restObjectMapper;

    @Bean(name = "restObjectMapper")
    public ObjectMapper restObjectMapper() {
        ObjectMapper jsonMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addSerializer(DateTime.class, new DateTimeSerializer("yyyy-MM-dd HH:mm:ss"));
        module.addDeserializer(DateTime.class, new DateTimeDeserializer("yyyy-MM-dd HH:mm:ss"));
        jsonMapper.registerModule(module);

        return jsonMapper;
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new TestRestTemplate();

        Integer jsonConverterIndex = null;
        for (int i = 0; i < restTemplate.getMessageConverters().size(); i++) {
            if(restTemplate.getMessageConverters().get(i).getClass().isAssignableFrom(MappingJackson2HttpMessageConverter.class)) {
                jsonConverterIndex = i;
                break;
            }
        }

        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonMessageConverter.setObjectMapper(restObjectMapper);

        if(jsonConverterIndex != null) {
            restTemplate.getMessageConverters().set(jsonConverterIndex, jsonMessageConverter);
        } else {
            restTemplate.getMessageConverters().add(jsonMessageConverter);
        }

        return restTemplate;
    }

}
