package vchernyak.mybank.web.controller.impl;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import vchernyak.mybank.MockedWebConfig;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.json.JacksonConfig;
import vchernyak.mybank.rules.data.input.impl.LoanApplicationInput;
import vchernyak.mybank.rules.data.input.impl.LoanExtensionInput;
import vchernyak.mybank.rules.data.result.RuleResult;
import vchernyak.mybank.rules.service.impl.LoanApplicationService;
import vchernyak.mybank.rules.service.impl.LoanExtensionService;
import vchernyak.mybank.security.auth.TokenAuthentication;
import vchernyak.mybank.web.ExceptionHandlerController;
import vchernyak.mybank.web.WebConfig;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class, JacksonConfig.class, ExceptionHandlerController.class, MockedWebConfig.class})
@WebAppConfiguration
public class LoansControllerTest {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private WebApplicationContext webContext;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private LoanApplicationService loanApplicationService;

    @Autowired
    private LoanExtensionService loanExtensionService;

    private MockMvc mockMvc;

    @BeforeClass
    public static void beforeClass() {
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    }

    @Before
    public void before() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build();

        TokenAuthentication authentication = mock(TokenAuthentication.class);
        User user = mock(User.class);

        when(authentication.getPrincipal()).thenReturn("user");
        when(userRepository.findByUsername(anyString())).thenReturn(user);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void after() {
        reset(userRepository, loanRepository, loanApplicationService, loanExtensionService);

        SecurityContextHolder.clearContext();
    }

    @Test
    public void loans200() throws Exception {
        when(loanRepository.findByUserOrderByStartAsc(any(User.class))).thenReturn(Collections.<Loan>emptyList());
        mockMvc.perform(get("/loans")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveLoanById200() throws Exception {
        when(loanRepository.findByUserAndId(any(User.class), anyLong())).thenReturn(mock(Loan.class));
        mockMvc.perform(get("/loans/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveLoanById404() throws Exception {
        when(loanRepository.findByUserAndId(any(User.class), anyLong())).thenReturn(null);
        mockMvc.perform(get("/loans/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void retrieveLoanByIdWithHistory200() throws Exception {
        when(loanRepository.findByUserAndId(any(User.class), anyLong())).thenReturn(mock(Loan.class));
        mockMvc.perform(get("/loans/1/history")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveLoanByIdWithHistory404() throws Exception {
        when(loanRepository.findByUserAndId(any(User.class), anyLong())).thenReturn(null);
        mockMvc.perform(get("/loans/1/history")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void applyLoan201() throws Exception {
        Loan loan = mock(Loan.class);
        when(loan.getId()).thenReturn(66l);

        RuleResult ruleResult = mock(RuleResult.class);
        when(ruleResult.isSuccess()).thenReturn(true);
        when(ruleResult.getResult()).thenReturn(loan);
        when(loanApplicationService.process(any(LoanApplicationInput.class))).thenReturn(ruleResult);

        MvcResult result = mockMvc.perform(post("/loans")
                .content("{\"amount\":10.0, \"end\":\"" + DateTime.now(DateTimeZone.UTC).plusDays(8).toString(DATE_FORMAT) + "\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        result.getAsyncResult();

        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/loans/66"));
    }

    @Test
    public void applyLoan400() throws Exception {
        RuleResult ruleResult = mock(RuleResult.class);
        when(ruleResult.isSuccess()).thenReturn(false);
        when(loanApplicationService.process(any(LoanApplicationInput.class))).thenReturn(ruleResult);

        MvcResult result = mockMvc.perform(post("/loans")
                .content("{\"amount\":10.0, \"end\":\"" + DateTime.now(DateTimeZone.UTC).plusDays(8).toString(DATE_FORMAT) + "\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        result.getAsyncResult();

        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void extendLoan201() throws Exception {
        when(loanRepository.findByUserAndId(any(User.class), anyLong())).thenReturn(mock(Loan.class));

        Loan loan = mock(Loan.class);
        when(loan.getId()).thenReturn(66l);

        RuleResult ruleResult = mock(RuleResult.class);
        when(ruleResult.isSuccess()).thenReturn(true);
        when(ruleResult.getResult()).thenReturn(loan);
        when(loanExtensionService.process(any(LoanExtensionInput.class))).thenReturn(ruleResult);

        MvcResult result = mockMvc.perform(post("/loans/1/extend")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        result.getAsyncResult();

        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/loans/66"));
    }

    @Test
    public void extendLoan404() throws Exception {
        when(loanRepository.findByUserAndId(any(User.class), anyLong())).thenReturn(null);
        MvcResult result = mockMvc.perform(post("/loans/1/extend")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        result.getAsyncResult();

        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isNotFound());
    }

    @Test
    public void extendLoan400() throws Exception {
        when(loanRepository.findByUserAndId(any(User.class), anyLong())).thenReturn(mock(Loan.class));

        RuleResult ruleResult = mock(RuleResult.class);
        when(ruleResult.isSuccess()).thenReturn(false);
        when(loanExtensionService.process(any(LoanExtensionInput.class))).thenReturn(ruleResult);

        MvcResult result = mockMvc.perform(post("/loans/1/extend")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        result.getAsyncResult();

        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isBadRequest());
    }

}
