package vchernyak.mybank.security.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import vchernyak.mybank.PropertiesConfig;
import vchernyak.mybank.json.JacksonConfig;
import vchernyak.mybank.json.security.Token;
import vchernyak.mybank.security.auth.SecurityUser;
import vchernyak.mybank.security.auth.TokenAuthentication;
import vchernyak.mybank.security.util.EncryptorDecryptor;
import vchernyak.mybank.security.util.InvalidTokenException;
import vchernyak.mybank.security.util.ResponseWriter;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PropertiesConfig.class, JacksonConfig.class, EncryptorDecryptor.class})
public class TokenServiceTest {

    @Value("${token.header}")
    private String authTokenHeaderName;

    @Value("${token.lifetime}")
    private Integer tokenLifeTime;

    @Autowired
    private EncryptorDecryptor encryptorDecryptor;

    @Autowired
    @Qualifier("jsonMapper")
    private ObjectMapper jsonMapper;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final ResponseWriter responseWriter = mock(ResponseWriter.class);

    private final HttpServletRequest request = mock(HttpServletRequest.class);

    private TokenService tokenService;

    @Before
    public void before() {
        tokenService = new TokenService(responseWriter, encryptorDecryptor, jsonMapper, authTokenHeaderName, tokenLifeTime);
    }

    @After
    public void after() {
        reset(responseWriter, request);
    }

    @Test
    public void deserializeLoginCredentials() throws IOException {
        doReturn(mockServletInputStream("{\"username\" : \"user\", \"password\" : \"password\"}")).when(request).getInputStream();

        UsernamePasswordAuthenticationToken token = tokenService.deserializeLoginCredentials(request);

        assertNotNull(token);
        assertEquals("user", token.getPrincipal());
        assertEquals("password", token.getCredentials());
    }

    @Test
    public void deserializeLoginCredentialsException() throws IOException {
        doReturn(mockServletInputStream("")).when(request).getInputStream();

        thrown.expect(BadCredentialsException.class);
        tokenService.deserializeLoginCredentials(request);
    }

    @Test
    public void writeAuthToken() throws BadPaddingException, IllegalBlockSizeException, IOException {
        SecurityUser securityUser = new SecurityUser("user", "password", DateTime.now(DateTimeZone.UTC));
        String serializedToken = jsonMapper.writeValueAsString(securityUser);
        String encryptedToken = encryptorDecryptor.encrypt(serializedToken);
        Token token = new Token(encryptedToken, securityUser.getExpirationTime());
        HttpServletResponse response = mock(HttpServletResponse.class);

        tokenService.writeAuthToken(response, securityUser);

        verify(responseWriter).writeToken(response, token);
    }

    @Test
    public void deserializeAuthToken() throws BadPaddingException, IllegalBlockSizeException, JsonProcessingException {
        SecurityUser securityUser = new SecurityUser("user", "password", DateTime.now(DateTimeZone.UTC).withMillis(0));
        String serializedToken = jsonMapper.writeValueAsString(securityUser);
        String encryptedToken = encryptorDecryptor.encrypt(serializedToken);
        TokenAuthentication tokenAuthentication = new TokenAuthentication(securityUser, "127.0.0.1");

        when(request.getHeader(authTokenHeaderName)).thenReturn(encryptedToken);
        when(request.getRemoteAddr()).thenReturn("127.0.0.1");

        assertEquals(tokenAuthentication, tokenService.deserializeAuthToken(request));
    }

    @Test
    public void deserializeAuthTokenException() throws BadPaddingException, IllegalBlockSizeException, JsonProcessingException {
        SecurityUser securityUser = new SecurityUser("user", "password", DateTime.now(DateTimeZone.UTC));
        String serializedToken = jsonMapper.writeValueAsString(securityUser);
        String encryptedToken = encryptorDecryptor.encrypt(serializedToken);

        when(request.getHeader(authTokenHeaderName)).thenReturn("blah" + encryptedToken);
        when(request.getRemoteAddr()).thenReturn("127.0.0.1");

        thrown.expect(InvalidTokenException.class);

        tokenService.deserializeAuthToken(request);
    }

    @Test
    public void containsToken() {
        when(request.getHeader(authTokenHeaderName))
                .thenReturn("some deader")
                .thenReturn(null);

        assertTrue(tokenService.containsToken(request));
        assertFalse(tokenService.containsToken(request));
    }

    @Test
    public void isTokenExpired() {
        DateTime expired = DateTime.now(DateTimeZone.UTC).minusSeconds(1);
        DateTime valid = expired.plusMinutes(1);

        assertTrue(tokenService.isTokenExpired(expired));
        assertFalse(tokenService.isTokenExpired(valid));
    }

    private ServletInputStream mockServletInputStream(String data) {
        try {
            byte[] binaryData = data.getBytes();
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(binaryData);

            ServletInputStream servletInputStream = mock(ServletInputStream.class);

            when(servletInputStream.read(Matchers.<byte[]>any(), anyInt(), anyInt())).thenAnswer(invocationOnMock -> {
                Object[] args = invocationOnMock.getArguments();
                byte[] output = (byte[]) args[0];
                int offset = (int) args[1];
                int length = (int) args[2];
                return byteArrayInputStream.read(output, offset, length);
            });

            return servletInputStream;
        } catch (IOException e) {
            return null;
        }
    }

}
