package vchernyak.mybank.security.service;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.security.auth.SecurityUser;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class UserDetailsServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void loadUserByUsername() {
        User user = mock(User.class);
        DateTime dateTime = DateTime.now(DateTimeZone.UTC);
        SecurityUser securityUser = new SecurityUser(user, dateTime);

        UserRepository userRepository = mock(UserRepository.class);
        TokenService tokenService = mock(TokenService.class);
        UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl(userRepository, tokenService);

        when(userRepository.findByUsername(anyString())).thenReturn(user);
        when(tokenService.prepareTokenExpirationTime()).thenReturn(dateTime);

        assertEquals(securityUser, userDetailsService.loadUserByUsername("some username"));
    }

    @Test
    public void loadUserByUsernameNotFound() {
        UserRepository userRepository = mock(UserRepository.class);
        TokenService tokenService = mock(TokenService.class);
        UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl(userRepository, tokenService);

        when(userRepository.findByUsername(anyString())).thenReturn(null);

        thrown.expect(UsernameNotFoundException.class);

        userDetailsService.loadUserByUsername("some username");
    }

}
