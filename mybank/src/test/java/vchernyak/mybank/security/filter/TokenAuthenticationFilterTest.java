package vchernyak.mybank.security.filter;

import org.junit.Test;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import vchernyak.mybank.security.auth.TokenAuthentication;
import vchernyak.mybank.security.service.TokenService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class TokenAuthenticationFilterTest {

    @Test
    public void doFilter() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);
        TokenAuthentication tokenAuth = mock(TokenAuthentication.class);

        TokenService tokenService = mock(TokenService.class);
        when(tokenService.containsToken(request)).thenReturn(true);
        when(tokenService.deserializeAuthToken(request)).thenReturn(tokenAuth);

        AuthenticationManager authenticationManager = mock(AuthenticationManager.class);
        when(authenticationManager.authenticate(tokenAuth)).thenReturn(tokenAuth);

        TokenAuthenticationFilter filter = new TokenAuthenticationFilter(tokenService, authenticationManager);
        filter.doFilter(request, response, chain);

        assertEquals(tokenAuth, SecurityContextHolder.getContext().getAuthentication());
        verify(chain).doFilter(request, response);
    }

    @Test
    public void doFilterNoToken() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);

        TokenService tokenService = mock(TokenService.class);
        when(tokenService.containsToken(request)).thenReturn(false);

        AuthenticationManager authenticationManager = mock(AuthenticationManager.class);

        TokenAuthenticationFilter filter = new TokenAuthenticationFilter(tokenService, authenticationManager);
        filter.doFilter(request, response, chain);

        assertNull(SecurityContextHolder.getContext().getAuthentication());
        verify(chain).doFilter(request, response);
    }
}
