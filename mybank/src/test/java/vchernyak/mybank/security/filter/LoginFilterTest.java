package vchernyak.mybank.security.filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import vchernyak.mybank.security.auth.SecurityUser;
import vchernyak.mybank.security.service.TokenService;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;


public class LoginFilterTest {

    private final HttpServletRequest request = mock(HttpServletRequest.class);

    private final HttpServletResponse response = mock(HttpServletResponse.class);

    private final TokenService tokenService = mock(TokenService.class);

    private final AuthenticationManager authenticationManager = mock(AuthenticationManager.class);

    private LoginFilter loginFilter;

    @Before
    public void before() {
        loginFilter = new LoginFilter(HttpMethod.POST, "/login", tokenService, authenticationManager);
    }

    @After
    public void after() {
        reset(request, response, tokenService, authenticationManager);
        SecurityContextHolder.clearContext();
    }

    @Test
    public void attemptAuthentication() throws IOException, ServletException {
        loginFilter.attemptAuthentication(request, response);

        verify(tokenService).deserializeLoginCredentials(request);
        verify(authenticationManager).authenticate(any(Authentication.class));
    }

    @Test
    public void successfulAuthentication() throws BadPaddingException, IOException, IllegalBlockSizeException, ServletException {
        SecurityUser user = mock(SecurityUser.class);
        Authentication auth = mock(Authentication.class);

        when(auth.getPrincipal()).thenReturn(user);

        loginFilter.successfulAuthentication(request, response, mock(FilterChain.class), auth);

        verify(tokenService).writeAuthToken(response, user);
    }

    @Test
    public void successfulAuthenticationException() throws BadPaddingException, IOException, IllegalBlockSizeException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(mock(Authentication.class));

        SecurityUser user = mock(SecurityUser.class);
        Authentication auth = mock(Authentication.class);

        when(auth.getPrincipal()).thenReturn(user);
        doThrow(BadPaddingException.class).when(tokenService).writeAuthToken(response, user);

        boolean exceptionThrown = false;
        try {
            loginFilter.successfulAuthentication(request, response, mock(FilterChain.class), auth);
        } catch (ServletException e) {
            exceptionThrown = true;
        }

        assertTrue(exceptionThrown);
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void unsuccessfulAuthentication() throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(mock(Authentication.class));

        boolean exceptionThrown = false;
        try {
            loginFilter.unsuccessfulAuthentication(request, response, mock(AuthenticationException.class));
        } catch (AuthenticationException e) {
            exceptionThrown = true;
        }

        assertTrue(exceptionThrown);
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }
}
