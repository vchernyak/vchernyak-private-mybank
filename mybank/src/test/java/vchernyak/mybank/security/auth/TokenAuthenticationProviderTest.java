package vchernyak.mybank.security.auth;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.security.service.TokenService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class TokenAuthenticationProviderTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final TokenAuthentication authentication = mock(TokenAuthentication.class);

    private final UserRepository userRepository = mock(UserRepository.class);

    private final TokenService tokenService = mock(TokenService.class);

    private TokenAuthenticationProvider tokenAuthenticationProvider;

    @Before
    public void before() {
        tokenAuthenticationProvider = new TokenAuthenticationProvider(userRepository, tokenService);
    }

    @After
    public void after() {
        reset(authentication, userRepository, tokenService);
    }

    @Test
    public void authenticate() {
        when(authentication.getPrincipal()).thenReturn("username");
        when(authentication.getExpirationTime()).thenReturn(DateTime.now(DateTimeZone.UTC));
        when(tokenService.isTokenExpired(any(DateTime.class))).thenReturn(false);
        when(userRepository.findByUsername("username")).thenReturn(mock(User.class));

        tokenAuthenticationProvider.authenticate(authentication);

        verify(authentication).setAuthenticated(true);
    }

    @Test
    public void authenticateBadCredentials() {
        when(authentication.getPrincipal()).thenReturn("username");
        when(userRepository.findByUsername("username")).thenReturn(null);

        thrown.expect(BadCredentialsException.class);

        tokenAuthenticationProvider.authenticate(authentication);
    }

    @Test
    public void authenticateExpired() {
        when(authentication.getPrincipal()).thenReturn("username");
        when(authentication.getExpirationTime()).thenReturn(DateTime.now(DateTimeZone.UTC));
        when(tokenService.isTokenExpired(any(DateTime.class))).thenReturn(true);
        when(userRepository.findByUsername("username")).thenReturn(mock(User.class));

        thrown.expect(CredentialsExpiredException.class);

        tokenAuthenticationProvider.authenticate(authentication);
    }

    @Test
    public void supports() {
        assertTrue(tokenAuthenticationProvider.supports(TokenAuthentication.class));
        assertFalse(tokenAuthenticationProvider.supports(Object.class));
    }
}
