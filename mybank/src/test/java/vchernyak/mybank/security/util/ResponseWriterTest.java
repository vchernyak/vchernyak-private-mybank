package vchernyak.mybank.security.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import vchernyak.mybank.PropertiesConfig;
import vchernyak.mybank.json.JacksonConfig;
import vchernyak.mybank.json.security.AuthenticationError;
import vchernyak.mybank.json.security.Token;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PropertiesConfig.class, JacksonConfig.class, ResponseWriter.class})
public class ResponseWriterTest {

    @Autowired
    private ResponseWriter responseWriter;

    @Autowired
    @Qualifier("jsonMapper")
    private ObjectMapper mapper;

    @Test
    public void writeTokenStatus() throws IOException {
        Token token = new Token("token", DateTime.now(DateTimeZone.UTC));

        HttpServletResponse response = mock(HttpServletResponse.class);
        PrintWriter pw = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(pw);

        responseWriter.writeToken(response, token);

        verify(response).setStatus(HttpServletResponse.SC_OK);
        verify(response).setContentType(MediaType.APPLICATION_JSON_VALUE);
        verify(response).setCharacterEncoding(StandardCharsets.UTF_8.name());
        verify(pw).append(mapper.writeValueAsString(token));
    }

    @Test
    public void writeAuthenticationError() throws IOException {
        AuthenticationException exception = new AuthenticationException("") {};

        HttpServletResponse response = mock(HttpServletResponse.class);
        PrintWriter pw = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(pw);

        responseWriter.writeAuthenticationError(response, exception);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(response).setContentType(MediaType.APPLICATION_JSON_VALUE);
        verify(response).setCharacterEncoding(StandardCharsets.UTF_8.name());
        verify(pw).append(mapper.writeValueAsString(new AuthenticationError(exception)));
    }
}
