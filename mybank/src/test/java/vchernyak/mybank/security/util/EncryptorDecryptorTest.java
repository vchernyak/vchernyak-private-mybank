package vchernyak.mybank.security.util;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import vchernyak.mybank.PropertiesConfig;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {PropertiesConfig.class, EncryptorDecryptor.class})
public class EncryptorDecryptorTest {

    @Autowired
    private EncryptorDecryptor encryptorDecryptor;

    @Test
    public void encryptionDecryption() throws BadPaddingException, IllegalBlockSizeException {
        String token = "token";
        String encryptedToken = encryptorDecryptor.encrypt(token);
        assertNotNull(encryptedToken);
        String decryptedToken = encryptorDecryptor.decrypt(encryptedToken);
        assertNotNull(decryptedToken);
        assertEquals(token, decryptedToken);
    }

}
