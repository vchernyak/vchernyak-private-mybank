package vchernyak.mybank.json;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.time.DateTime;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.json.usertype.DateTimeDeserializer;
import vchernyak.mybank.json.usertype.DateTimeSerializer;
import vchernyak.mybank.json.security.LoginCredentialsMixin;
import vchernyak.mybank.json.security.SecurityUserMixin;
import vchernyak.mybank.json.web.LoanMixin;
import vchernyak.mybank.security.auth.SecurityUser;

/**
 * Configuration of Jackson {@link com.fasterxml.jackson.databind.ObjectMapper} that is used for JSON
 */
@Configuration
public class JacksonConfig {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Bean(name = "jsonMapper")
    public ObjectMapper jsonMapper() {
        ObjectMapper jsonMapper = new ObjectMapper();
        jsonMapper.disable(MapperFeature.AUTO_DETECT_GETTERS, MapperFeature.AUTO_DETECT_IS_GETTERS, MapperFeature.AUTO_DETECT_FIELDS);

        //DateTime serialization/deserialization
        SimpleModule module = new SimpleModule();
        module.addSerializer(DateTime.class, new DateTimeSerializer(DATE_FORMAT));
        module.addDeserializer(DateTime.class, new DateTimeDeserializer(DATE_FORMAT));
        jsonMapper.registerModule(module);

        //security objects mapping
        jsonMapper.addMixInAnnotations(UsernamePasswordAuthenticationToken.class, LoginCredentialsMixin.class);
        jsonMapper.addMixInAnnotations(SecurityUser.class, SecurityUserMixin.class);

        //data objects mapping
        jsonMapper.addMixInAnnotations(Loan.class, LoanMixin.class);

        return jsonMapper;
    }
}
