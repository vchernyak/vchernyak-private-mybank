package vchernyak.mybank.json.security;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.joda.time.DateTime;

/**
 * Mixin to map {@link vchernyak.mybank.security.auth.SecurityUser} to JSON
 */
@JsonPropertyOrder({"username", "expirationTime"})
@JsonIgnoreProperties(ignoreUnknown = true, value = {"password", "authorities", "enabled", "accountNonLocked", "accountNonExpired", "credentialsNonExpired"})
public abstract class SecurityUserMixin {

    @JsonProperty("username")
    abstract String getUsername();

    @JsonProperty("expirationTime")
    abstract DateTime getExpirationTime();

    @JsonCreator
    SecurityUserMixin(@JsonProperty(value = "username", required = true) String username,
                      @JsonProperty("password") String password,
                      @JsonProperty(value = "expirationTime", required = true) DateTime expirationTime){}

}
