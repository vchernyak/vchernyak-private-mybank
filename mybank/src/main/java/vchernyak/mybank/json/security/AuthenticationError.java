package vchernyak.mybank.json.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import vchernyak.mybank.security.util.InvalidTokenException;

/**
 * Represents Authentication error details class, that serializes to response
 */
@JsonPropertyOrder({"reason", "message"})
public class AuthenticationError {

    private enum Reason { BAD_CREDENTIALS, INVALID_TOKEN, TOKEN_EXPIRED, AUTHENTICTION_REQUIRED, REASON_UNKNOWN }

    @JsonProperty("reason")
    private final Reason reason;

    @JsonProperty("message")
    private final String message;

    public AuthenticationError(AuthenticationException authException) {
        this.message = authException.getMessage();

        if(BadCredentialsException.class.isAssignableFrom(authException.getClass())) {
            reason = Reason.BAD_CREDENTIALS;
        } else if(InvalidTokenException.class.isAssignableFrom(authException.getClass())) {
            reason = Reason.INVALID_TOKEN;
        } else if(CredentialsExpiredException.class.isAssignableFrom(authException.getClass())) {
            reason = Reason.TOKEN_EXPIRED;
        } else if(InsufficientAuthenticationException.class.isAssignableFrom(authException.getClass())
                || AuthenticationCredentialsNotFoundException.class.isAssignableFrom(authException.getClass())) {
            reason = Reason.AUTHENTICTION_REQUIRED;
        } else {
            reason = Reason.REASON_UNKNOWN;
        }
    }

}
