package vchernyak.mybank.json.security;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Mixin to map credentials to {@link org.springframework.security.authentication.UsernamePasswordAuthenticationToken}
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class LoginCredentialsMixin {

    /**
     * Maps credentials to constructor <code>UsernamePasswordAuthenticationToken(Object principal, Object credentials)</code>
     */
    @JsonCreator
    LoginCredentialsMixin(@JsonProperty(value = "username") Object principal,
                          @JsonProperty(value = "password") Object credentials){}

}
