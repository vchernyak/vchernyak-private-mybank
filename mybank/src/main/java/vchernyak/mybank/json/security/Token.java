package vchernyak.mybank.json.security;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;

/**
 * Used to write token to response after successfull authentication
 */
@JsonPropertyOrder({"token", "expirationTime"})
public class Token {

    @JsonProperty("token")
    private String token;

    @JsonProperty("expirationTime")
    private DateTime expirationTime;

    @JsonCreator
    public Token(@JsonProperty("token") String token, @JsonProperty("expirationTime") DateTime expirationTime) {
        this.token = token;
        this.expirationTime = expirationTime;
    }

    public DateTime getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(DateTime expirationTime) {
        this.expirationTime = expirationTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
