package vchernyak.mybank.json.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Represents user's loan application request json
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanApplication {

    @JsonProperty("amount")
    private Double amount;

    @JsonProperty("end")
    private DateTime end;

    public Double getAmount() {
        return amount;
    }

    public DateTime getEnd() {
        return end;
    }
}
