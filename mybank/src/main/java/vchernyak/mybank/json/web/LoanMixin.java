package vchernyak.mybank.json.web;

import com.fasterxml.jackson.annotation.*;
import org.joda.time.DateTime;
import vchernyak.mybank.dao.entity.Loan;

/**
 * Mixin to map {@link vchernyak.mybank.dao.entity.Loan} for desired JSON response
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"id", "amount", "interest", "start", "end", "closed", "extendedId", "extensionId"})
public interface LoanMixin {

    @JsonProperty("id")
    Long getId();

    @JsonProperty("amount")
    double getAmount();

    @JsonProperty("interest")
    double getInterest();

    @JsonProperty("start")
    DateTime getStart();

    @JsonProperty("end")
    DateTime getEnd();

    @JsonProperty("closed")
    boolean isClosed();

    @JsonProperty("extendedId")
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    Loan getExtended();

    @JsonProperty("extensionId")
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    Loan getExtension();


}
