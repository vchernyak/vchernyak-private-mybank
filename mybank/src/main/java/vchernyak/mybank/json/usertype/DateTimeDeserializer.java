package vchernyak.mybank.json.usertype;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;

/**
 * Jackson deserializer for {@link org.joda.time.DateTime}
 */
public class DateTimeDeserializer extends JsonDeserializer<DateTime> {

    private final String dateFormat;

    public DateTimeDeserializer(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public DateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        return DateTime.parse(jp.getText(), DateTimeFormat.forPattern(dateFormat)).withZoneRetainFields(DateTimeZone.UTC);
    }
}
