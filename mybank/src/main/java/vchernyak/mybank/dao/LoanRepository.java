package vchernyak.mybank.dao;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.dao.entity.User;

import java.util.List;

/**
 * {@link Loan} repository
 */
@Repository
public interface LoanRepository extends PagingAndSortingRepository<Loan, Long> {

    List<Loan> findByUserOrderByStartAsc(User user);

    List<Loan> findByUserOrderByEndAsc(User user);

    Loan findByUserAndId(User user, Long id);

}
