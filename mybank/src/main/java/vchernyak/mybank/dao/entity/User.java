package vchernyak.mybank.dao.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String username;

    @NotNull
    private String password;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Loan> loans;

    User(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Loan> getLoans() {
        return loans;
    }

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }

    public static class Builder {

        private final User user;

        public Builder() {
            this.user = new User();
        }

        public User build() {
            return user;
        }

        public Builder withUsername(String username) {
            user.setUsername(username);
            return this;
        }

        public Builder withPassword(String password) {
            user.setPassword(password);
            return this;
        }
    }
}
