package vchernyak.mybank.dao.entity;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class UserActionInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @Column(updatable = false)
    private DateTime actionDate;

    @NotNull
    @Size(min = 7, max = 49)
    @Column(updatable = false)
    private String ip;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false)
    private User user;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false)
    private Loan loan;

    UserActionInfo(){}

    @PrePersist
    private void prePersist() {
        if(actionDate == null) {
            actionDate = DateTime.now(DateTimeZone.UTC);
        }
        if(user == null && loan != null) {
            user = loan.getUser();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getActionDate() {
        return actionDate;
    }

    public void setActionDate(DateTime actionDate) {
        this.actionDate = actionDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public static class Builder {

        private final UserActionInfo userActionInfo;

        public Builder() {
            this.userActionInfo = new UserActionInfo();
        }

        public UserActionInfo build() {
            return userActionInfo;
        }

        public Builder withActionDate(DateTime actionDate) {
            userActionInfo.setActionDate(actionDate);
            return this;
        }

        public Builder withIp(String ip) {
            userActionInfo.setIp(ip);
            return this;
        }

        public Builder withUser(User user) {
            userActionInfo.setUser(user);
            return this;
        }

        public Builder withLoan(Loan loan) {
            userActionInfo.setLoan(loan);
            return this;
        }


    }
}
