package vchernyak.mybank.dao.entity;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @Column(updatable = false)
    private double amount;

    @NotNull
    @Column(updatable = false)
    private double interest;

    @NotNull
    @Column(updatable = false)
    private DateTime start;

    @NotNull
    private DateTime end;

    @NotNull
    private boolean closed;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false)
    private User user;

    @OneToOne(fetch = FetchType.LAZY)
    private Loan extended;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "extended", cascade = {CascadeType.ALL})
    @JoinColumn(updatable = false)
    private Loan extension;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "loan", cascade = {CascadeType.ALL})
    private UserActionInfo userActionInfo;

    Loan(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Loan getExtended() {
        return extended;
    }

    public void setExtended(Loan extended) {
        this.extended = extended;
    }

    public Loan getExtension() {
        return extension;
    }

    public void setExtension(Loan extension) {
        this.extension = extension;
    }

    public UserActionInfo getUserActionInfo() {
        return userActionInfo;
    }

    public void setUserActionInfo(UserActionInfo userActionInfo) {
        this.userActionInfo = userActionInfo;
    }

    public static class Builder {

        private final Loan loan;

        public Builder() {
            loan = new Loan();
        }

        public Loan build() {
            return loan;
        }

        public Builder withAmount(double amount) {
            loan.setAmount(amount);
            return this;
        }

        public Builder withInterest(double interest) {
            loan.setInterest(interest);
            return this;
        }

        public Builder withStart(DateTime start) {
            loan.setStart(start);
            return this;
        }

        public Builder withEnd(DateTime end) {
            loan.setEnd(end);
            return this;
        }

        public Builder withClosed(boolean closed) {
            loan.setClosed(closed);
            return this;
        }

        public Builder withUser(User user) {
            loan.setUser(user);
            return this;
        }

        public Builder withExtended(Loan extended) {
            loan.setExtended(extended);
            if(extended != null && extended.getId() == null) {
                extended.setExtension(loan);
            }
            return this;
        }

        public Builder withUserActionInfo(UserActionInfo userActionInfo) {
            loan.setUserActionInfo(userActionInfo);
            if(userActionInfo != null && userActionInfo.getId() == null) {
                userActionInfo.setLoan(loan);
            }
            return this;
        }

        public Builder withUserActionInfo(String ip) {
            UserActionInfo userActionInfo = new UserActionInfo.Builder()
                    .withUser(loan.getUser())
                    .withIp(ip)
                    .withLoan(loan)
                    .withActionDate(DateTime.now(DateTimeZone.UTC))
            .build();
            loan.setUserActionInfo(userActionInfo);
            return this;
        }


    }
}
