package vchernyak.mybank;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.dao.entity.User;

/**
 * Application startup class.
 * Starts application and inserts 3 users into db:<br/>
 *      username: "user1", password: "user1"<br/>
 *      username: "user2", password: "user2"<br/>
 *      username: "user3", password: "user3"<br/>
 */
public class Application {

    /**
     * Main method.
     * @param args spring boot parameters only supported, i.e. --server.port=8081
     */
    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationConfig.class, args);
        populateDatabase(context);
    }

    /**
     * Populates DB with 3 users
     * @param context application context
     */
    private static void populateDatabase(ConfigurableApplicationContext context) {
        UserRepository userRepository = context.getBean(UserRepository.class);
        PasswordEncoder passwordEncoder = context.getBean(PasswordEncoder.class);

        insertUser(userRepository, "user1", passwordEncoder.encode("user1"));
        insertUser(userRepository, "user2", passwordEncoder.encode("user2"));
        insertUser(userRepository, "user3", passwordEncoder.encode("user3"));
    }

    /**
     * Helping method to create user and store into db
     * @param userRepository userRepository bean
     * @param username username
     * @param password encrypted password
     */
    private static void insertUser(UserRepository userRepository, String username, String password) {
        User user = new User.Builder()
                .withUsername(username)
                .withPassword(password)
                .build();

        userRepository.save(user);
    }

}
