package vchernyak.mybank.security.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import vchernyak.mybank.json.security.AuthenticationError;
import vchernyak.mybank.json.security.Token;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Utility that writes to response.<br />
 * Used to write token from filter, or error from auth entry point
 */
@Component
public class ResponseWriter {

    @Autowired
    @Qualifier("jsonMapper")
    private ObjectMapper jsonMapper;

    /**
     * Writes auth error to response: status 401, error serialized to JSON
     */
    public void writeAuthenticationError(HttpServletResponse response, AuthenticationException error) throws IOException {
        writeResponse(response, HttpServletResponse.SC_UNAUTHORIZED, new AuthenticationError(error));
    }

    /**
     * Writes auth token to response: status 200, token serialized to JSON
     */
    public void writeToken(HttpServletResponse response, Token token) throws IOException {
        writeResponse(response, HttpServletResponse.SC_OK, token);
    }

    private void writeResponse(HttpServletResponse response, int status, Object jsonPojo) throws IOException {
        response.setStatus(status);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());

        response.getWriter().append(jsonMapper.writeValueAsString(jsonPojo));
    }
}
