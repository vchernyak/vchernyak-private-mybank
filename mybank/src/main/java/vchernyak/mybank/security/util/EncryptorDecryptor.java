package vchernyak.mybank.security.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.crypto.*;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Utility for encryption and decryption. Algorythm is defined in appilcation.properties, secret key generates when Spring is up.
 */
@Component
public class EncryptorDecryptor {

    private static final Logger log = LoggerFactory.getLogger(EncryptorDecryptor.class);

    @Value("${encryption.algorythm}")
    private String algorythm;

    private Cipher encryptor;

    private Cipher decryptor;

    /**
     * Initialization. Generation of secret key, creating instances of encryptor and decryptor.
     */
    @PostConstruct
    private void init() {
        try {
            SecretKey key = KeyGenerator.getInstance(algorythm).generateKey();

            encryptor = Cipher.getInstance(algorythm);
            decryptor = Cipher.getInstance(algorythm);

            encryptor.init(Cipher.ENCRYPT_MODE, key);
            decryptor.init(Cipher.DECRYPT_MODE, key);
        } catch (NoSuchAlgorithmException|InvalidKeyException|NoSuchPaddingException e) {
            log.error("Failed to initialize encryption utility", e);
        }
    }

    /**
     * Encrypts given string
     * @param toEncrypt string to encrypt
     * @return encrypted string in base64 format
     */
    public synchronized String encrypt(String toEncrypt) throws BadPaddingException, IllegalBlockSizeException {
        byte[] encrypted = encryptor.doFinal(toEncrypt.getBytes(StandardCharsets.UTF_8));
        return Base64Utils.encodeToString(encrypted);
    }

    /**
     * Decrypts given string
     * @param toDecrypt string to decrypt in base64 format
     * @return decrypted string
     */
    public synchronized String decrypt(String toDecrypt) throws BadPaddingException, IllegalBlockSizeException {
        byte[] encrypted = Base64Utils.decodeFromString(toDecrypt);
        return new String(decryptor.doFinal(encrypted), StandardCharsets.UTF_8);
    }

}
