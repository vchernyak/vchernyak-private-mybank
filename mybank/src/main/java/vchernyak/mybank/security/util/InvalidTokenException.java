package vchernyak.mybank.security.util;

import org.springframework.security.core.AuthenticationException;

/**
 * Separate exception class for invalid token error.<br />
 * Created as separate exception to distinct among other errors in {@link vchernyak.mybank.json.security.AuthenticationError}
 */
public class InvalidTokenException extends AuthenticationException {

    public InvalidTokenException(String msg) {
        super(msg);
    }

}
