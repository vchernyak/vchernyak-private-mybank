package vchernyak.mybank.security.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import vchernyak.mybank.json.security.Token;
import vchernyak.mybank.security.util.EncryptorDecryptor;
import vchernyak.mybank.security.util.InvalidTokenException;
import vchernyak.mybank.security.auth.SecurityUser;
import vchernyak.mybank.security.auth.TokenAuthentication;
import vchernyak.mybank.security.util.ResponseWriter;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Utility that works with authentication tokens: reads from header, writes to response, checks if expired
 */
@Service
public class TokenService {

    private static final Logger log = LoggerFactory.getLogger(TokenService.class);

    private final String authTokenHeaderName;

    private final Integer tokenLifeTime;

    private final ResponseWriter responseWriter;

    private final EncryptorDecryptor encryptorDecryptor;

    private final ObjectMapper jsonMapper;

    @Autowired
    public TokenService(ResponseWriter responseWriter, EncryptorDecryptor encryptorDecryptor, @Qualifier("jsonMapper") ObjectMapper jsonMapper,
                        @Value("${token.header}") String authTokenHeaderName, @Value("${token.lifetime}") Integer tokenLifeTime) {
        this.responseWriter = responseWriter;
        this.encryptorDecryptor = encryptorDecryptor;
        this.jsonMapper = jsonMapper;
        this.authTokenHeaderName = authTokenHeaderName;
        this.tokenLifeTime = tokenLifeTime;
    }

    /**
     * Reads credentials from request
     * @param request request
     * @return serialized credentionals
     * @throws BadCredentialsException could nor read/deserialize
     */
    public UsernamePasswordAuthenticationToken deserializeLoginCredentials(HttpServletRequest request) throws BadCredentialsException {
        try {
            return jsonMapper.readValue(request.getInputStream(), UsernamePasswordAuthenticationToken.class);
        } catch (Exception e) {
            log.info("Error while parsing credentials", e);
            throw new BadCredentialsException("Bad credentials: Invalid input");
        }
    }

    /**
     * Writes encrypted token to response
     * @param response response
     * @param user security user, authenticated through appropriate dao auth provider
     */
    public void writeAuthToken(HttpServletResponse response, SecurityUser user) throws IOException, BadPaddingException, IllegalBlockSizeException {
        String serializedToken = jsonMapper.writeValueAsString(user);
        String encryptedToken = encryptorDecryptor.encrypt(serializedToken);

        Token token = new Token(encryptedToken, user.getExpirationTime());
        responseWriter.writeToken(response, token);
    }

    /**
     * Reads and decrypts encrypted token from request header
     * @param request request
     * @return {@link TokenAuthentication} instance, that will be passed to authentication provider
     * @throws InvalidTokenException could not read/parse/decrypt token
     */
    public TokenAuthentication deserializeAuthToken(HttpServletRequest request) throws InvalidTokenException {
        try {
            String decryptedToken = encryptorDecryptor.decrypt(request.getHeader(authTokenHeaderName));
            SecurityUser user = jsonMapper.readValue(decryptedToken, SecurityUser.class);

            return new TokenAuthentication(user, request.getRemoteAddr());
        } catch (Exception e) {
            log.info("Error while parsing token", e);
            throw new InvalidTokenException("Invalid token");
        }
    }

    /**
     * Checks if request contains auth header
     */
    public boolean containsToken(HttpServletRequest request) {
        return request.getHeader(authTokenHeaderName) != null;
    }

    /**
     * Prepares expiration time of token
     */
    public DateTime prepareTokenExpirationTime() {
        return DateTime.now(DateTimeZone.UTC).plusMinutes(tokenLifeTime);
    }

    /**
     * checks if tokenExpirationTime is before now (expired), or after (not expired)
     */
    public boolean isTokenExpired(DateTime tokenExpirationTime) {
        return tokenExpirationTime.isBefore(DateTime.now(DateTimeZone.UTC));
    }
}
