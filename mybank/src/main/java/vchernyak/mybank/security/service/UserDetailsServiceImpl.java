package vchernyak.mybank.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.security.auth.SecurityUser;

/**
 * Implemetation of {@link org.springframework.security.core.userdetails.UserDetailsService},
 * used in dao authentication layer.<br />
 * {@inheritDoc}
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    private final TokenService tokenService;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository, TokenService tokenService) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return new SecurityUser(user, tokenService.prepareTokenExpirationTime());
    }

}
