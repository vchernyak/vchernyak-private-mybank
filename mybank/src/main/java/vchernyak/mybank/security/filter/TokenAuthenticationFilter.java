package vchernyak.mybank.security.filter;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import vchernyak.mybank.security.service.TokenService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Token authentication filter.<br />
 * Mapped for every path. If appropriate header found in request, tries to authenticated. If auth successfull,
 * writes appropriate object to Spring Security context and sends request further through filter chain
 */
public class TokenAuthenticationFilter extends GenericFilterBean {

    private final TokenService tokenService;

    private final AuthenticationManager authenticationManager;

    public TokenAuthenticationFilter(TokenService tokenService, AuthenticationManager authenticationManager) {
        this.tokenService = tokenService;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(tokenService.containsToken((HttpServletRequest) request)) {
            Authentication auth = authenticationManager.authenticate(tokenService.deserializeAuthToken((HttpServletRequest) request));
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        chain.doFilter(request, response);
    }

}
