package vchernyak.mybank.security.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import vchernyak.mybank.security.auth.SecurityUser;
import vchernyak.mybank.security.service.TokenService;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Login filter.<br />
 * Maps to specific uri and method, reads JSON credentionals <br />
 * {"username":"$username","password":"$password"}<br />
 * and pass them to authetication manager.<br />
 * If authentication is successfull, writes encrypted token to response.
 */
public class LoginFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger log = LoggerFactory.getLogger(LoginFilter.class);

    private final TokenService tokenService;

    /**
     * Constructor
     * @param loginMethod mapped method
     * @param loginUrl mapped url
     * @param tokenService tokenService
     * @param authenticationManager authenticationManager
     */
    public LoginFilter(HttpMethod loginMethod, String loginUrl, TokenService tokenService, AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher(loginUrl, loginMethod.toString()));
        this.tokenService = tokenService;
        setAuthenticationManager(authenticationManager);
    }

    /**
     * Deserializes credentionals from JSON, gives them to authentication manager
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        return getAuthenticationManager().authenticate(tokenService.deserializeLoginCredentials(request));
    }

    /**
     * Writes encrypted token to response without going further through filter chain.<br />
     * May throw an exception during encryption (normally should never do this)
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        try {
            tokenService.writeAuthToken(response, (SecurityUser) authResult.getPrincipal());
        } catch (BadPaddingException|IllegalBlockSizeException e) {
            //normally this code should be unreachable
            log.error("Failed to encrypt token", e);

            SecurityContextHolder.clearContext();
            throw new ServletException("Failed to encrypt token", e);
        }
    }

    /**
     * Clears security context. Throws exception, that will be handled with configured {@link org.springframework.security.web.AuthenticationEntryPoint}
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        //clearing context without calling super method
        SecurityContextHolder.clearContext();

        throw failed;
    }
}
