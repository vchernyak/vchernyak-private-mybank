package vchernyak.mybank.security.auth;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import vchernyak.mybank.security.auth.SecurityUser;

/**
 * Implementation of {@link AbstractAuthenticationToken}<br />
 * Is created in {@link vchernyak.mybank.security.filter.TokenAuthenticationFilter} from token,
 * processed through auth checks in {@link vchernyak.mybank.security.auth.TokenAuthenticationProvider} and lives in Spring Security context.
 */
public class TokenAuthentication extends AbstractAuthenticationToken {

    private final String username;

    private final String ip;

    private final DateTime expirationTime;

    public TokenAuthentication(SecurityUser user, String ip) {
        super(user.getAuthorities());
        this.username = user.getUsername();
        this.ip = ip;
        this.expirationTime = user.getExpirationTime();
    }

    /**
     * @return null (password never saved in memory)
     */
    @Override
    public String getCredentials() {
        //not saving password
        return null;
    }

    /**
     * @return username
     */
    @Override
    public String getPrincipal() {
        return username;
    }

    /**
     * @return token expiration time
     */
    public DateTime getExpirationTime() {
        return expirationTime;
    }

    /**
     * @return ip of client
     */
    public String getIp() {
        return ip;
    }

    @Override
    public String toString() {
        return "[username: " + username + ", ip: " + ip + ", expirationTime: " + expirationTime.toString() + "]";
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
