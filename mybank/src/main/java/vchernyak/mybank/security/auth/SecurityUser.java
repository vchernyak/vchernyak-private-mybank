package vchernyak.mybank.security.auth;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.joda.time.DateTime;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import vchernyak.mybank.dao.entity.User;

import java.util.*;

/**
 * Implementation of {@link org.springframework.security.core.userdetails.UserDetails} that is used in Spring Security dao authentication provider.<br />
 * Used for authentication by credentials in {@link vchernyak.mybank.security.filter.LoginFilter}.
 * If authenticated became converted to JSON, encrypted and returned to client as token.
 */
public class SecurityUser implements UserDetails {

    //only USER role supported
    //every authenticated user has USER role
    private static final List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));

    private final String username;

    private final String password;

    private final DateTime expirationTime;

    public SecurityUser(String username, String password, DateTime expirationTime) {
        this.username = username;
        this.password = password;
        this.expirationTime = expirationTime;
    }

    public SecurityUser(User user, DateTime expirationTime) {
         this(user.getUsername(), user.getPassword(), expirationTime);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public DateTime getExpirationTime() {
        return expirationTime;
    }

    @Override
    public boolean isAccountNonExpired() {
        //account expiration not supported
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        //account locking not supported
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //credentials expiration not supported
        return true;
    }

    @Override
    public boolean isEnabled() {
        //account disabling not supported
        return true;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
