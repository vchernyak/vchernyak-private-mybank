package vchernyak.mybank.security.auth;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.security.service.TokenService;

/**
 * Implementation of {@link AuthenticationProvider} that supports {@link TokenAuthentication}
 */
@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private final UserRepository userRepository;

    private final TokenService tokenService;

    @Autowired
    public TokenAuthenticationProvider(UserRepository userRepository, TokenService tokenService) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
    }

    /**
     * Checks if user still exists in DB and if token is not expired.<br /><br />
     * {@inheritDoc}
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        User user = userRepository.findByUsername((String) authentication.getPrincipal());
        if(user == null) {
            throw new BadCredentialsException("Bad credentials");
        } else {
            DateTime tokenExpirationTime = ((TokenAuthentication) authentication).getExpirationTime();
            if(tokenService.isTokenExpired(tokenExpirationTime)) {
                throw new CredentialsExpiredException("Token expired");
            }
        }

        authentication.setAuthenticated(true);
        return authentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return TokenAuthentication.class.isAssignableFrom(authentication);
    }

}
