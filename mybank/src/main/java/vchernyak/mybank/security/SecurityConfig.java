package vchernyak.mybank.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import vchernyak.mybank.security.auth.TokenAuthenticationProvider;
import vchernyak.mybank.security.filter.LoginFilter;
import vchernyak.mybank.security.filter.TokenAuthenticationFilter;
import vchernyak.mybank.security.service.TokenService;
import vchernyak.mybank.security.service.UserDetailsServiceImpl;
import vchernyak.mybank.security.util.ResponseWriter;

/**
 * Configuration of Spring Security
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private ResponseWriter responseWriter;

    @Autowired
    private TokenAuthenticationProvider tokenAuthenticationProvider;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    SecurityConfig() {
        //disable defaults
        super(true);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //authentication entry point. writes appropriate auth error status and message
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
                //integration with servlet api
                .and().servletApi()
                //tells client not to use cache with setting appropriate headers
                .and().headers().cacheControl()
                //disable anonymous globally
                .and().anonymous().disable()
                //any request should be authenticated
                .authorizeRequests().anyRequest().authenticated()

                .and()
                // token based authentication
                .addFilterBefore(new TokenAuthenticationFilter(tokenService, authenticationManager()), FilterSecurityInterceptor.class)
                // json based login by POST of {"username":"$username","password":"$password"}, writes token to response
                .addFilterBefore(new LoginFilter(HttpMethod.POST, "/login", tokenService, authenticationManager()), TokenAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService())
                .passwordEncoder(passwordEncoder)
                .and()
                .authenticationProvider(tokenAuthenticationProvider);
    }

    @Override
    protected UserDetailsServiceImpl userDetailsService() {
        return userDetailsService;
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return (request, response, authException) -> responseWriter.writeAuthenticationError(response, authException);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(4);
    }

}
