package vchernyak.mybank.rules;

import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * Rules system spring config.<br />
 * Initializes located in META-INF/kmodule.xml knowledge bases, registers them as singleton beans.
 */
@Configuration
public class RulesConfig implements BeanDefinitionRegistryPostProcessor {

    /**
     * Initialization of kie knowledge base.
     * Introducing of kie bases to spring context as singletons by name.
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        for (String kieBaseName : kieContainer.getKieBaseNames()) {
            KieBase kieBase = kieContainer.getKieBase(kieBaseName);
            beanFactory.registerSingleton(kieBaseName, kieBase);
        }
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {}

    /**
     * Initializes KieSession's from templates for a first time.
     * It's time consuming operation, better to initialize at startup than on user request
     */
    @Bean
    public InitializingBean initializeKieSessions() {
        return new InitializingBean() {

            @Autowired
            @Qualifier("loanApplicationKieBase")
            private KieBase loanApplicationKieBase;

            @Autowired
            @Qualifier("loanExtensionKieBase")
            private KieBase loanExtensionKieBase;

            @Override
            public void afterPropertiesSet() throws Exception {
                StatelessKieSession kieSession = loanApplicationKieBase.newStatelessKieSession();
                kieSession.execute(CommandFactory.newBatchExecution(Arrays.asList(CommandFactory.newFireAllRules())));

                kieSession = loanExtensionKieBase.newStatelessKieSession();
                kieSession.execute(CommandFactory.newBatchExecution(Arrays.asList(CommandFactory.newFireAllRules())));
            }

        };
    }

}
