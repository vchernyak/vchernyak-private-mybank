package vchernyak.mybank.rules.log;

import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Listener of drools execution events.
 * In current implementation added logging of names of executed rules
 */
@Component
public class EventListenerLogger extends DefaultAgendaEventListener {

    private static final Map<String, Logger> loggers = new HashMap<>();

    /**
     * Logs rule name after it's facts matched
     */
    @Override
    public void afterMatchFired(final AfterMatchFiredEvent event) {
        LoggerFactory.getLogger(event.getMatch().getRule().getPackageName()).info("{} fired", event.getMatch().getRule().getName());
    }

}
