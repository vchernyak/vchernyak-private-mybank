package vchernyak.mybank.rules.service;

import org.kie.api.KieBase;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.kie.internal.command.CommandFactory;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.rules.data.input.RuleInput;
import vchernyak.mybank.rules.data.result.RuleResult;
import vchernyak.mybank.rules.log.EventListenerLogger;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generic rule service.<br />
 * To create implementation of new flow you just need create appropriate implementation of {@link vchernyak.mybank.rules.data.input.RuleInput} and implement {@link vchernyak.mybank.rules.service.impl.LoanApplicationService#kieBase} to return appropriate kiebase
 */
public abstract class RulesService<T, K extends RuleInput<T>> {

    protected static final String QUERY_SUCCESS = "success";
    protected static final String QUERY_MESSAGES = "message";
    protected static final String GLOBAL_LOAN_REPOSITORY = "loanRepository";

    private final LoanRepository loanRepository;

    private final EventListenerLogger eventListenerLogger;

    public RulesService(EventListenerLogger eventListenerLogger, LoanRepository loanRepository) {
        this.eventListenerLogger = eventListenerLogger;
        this.loanRepository = loanRepository;
    }

    /**
     * Processes input through rules, returns result
     * @param ruleInput rule input
     * @return result of rules execution
     */
    public RuleResult<T> process(K ruleInput) {
        List<Command> commands = prepareCommands(ruleInput);
        StatelessKieSession session = newSession();

        session.setGlobal(GLOBAL_LOAN_REPOSITORY, loanRepository);

        ExecutionResults execResults = session.execute(CommandFactory.newBatchExecution(commands));

        return handleResult(execResults, ruleInput);
    }

    /**
     * Prepares commands for drools
     * @param ruleInput rule input
     * @return list of commands to execute
     */
    protected List<Command> prepareCommands(K ruleInput) {
        List<Command> commands = ruleInput.facts().stream().map(CommandFactory::newInsert).collect(Collectors.toList());

        commands.add(CommandFactory.newFireAllRules());
        commands.add(CommandFactory.newQuery(QUERY_SUCCESS, QUERY_SUCCESS));
        commands.add(CommandFactory.newQuery(QUERY_MESSAGES, QUERY_MESSAGES));
        commands.add(CommandFactory.newQuery(ruleInput.resultQuery(), ruleInput.resultQuery()));

        return commands;
    }

    /**
     * Processes result of rules execution. Creates result object.
     * @param execResults results of execution
     * @param ruleInput rule input
     * @return resulting object
     */
    protected RuleResult<T> handleResult(ExecutionResults execResults, K ruleInput) {
        Boolean success = false;
        List<String> messages = new ArrayList<>();
        T resultObj = null;

        QueryResults qResults = (QueryResults) execResults.getValue(QUERY_SUCCESS);
        for (QueryResultsRow row : qResults) {
            success = (Boolean) row.get(QUERY_SUCCESS);
            break;
        }
        qResults = (QueryResults) execResults.getValue(QUERY_MESSAGES);
        for (QueryResultsRow row : qResults) {
            messages.add((String) row.get(QUERY_MESSAGES));
        }

        qResults = (QueryResults) execResults.getValue(ruleInput.resultQuery());
        for (QueryResultsRow row : qResults) {
            resultObj = (T) row.get(ruleInput.resultQuery());
            break;
        }

        return new RuleResult<>(success, messages, resultObj);
    }

    /**
     * Creates new stateless kie session
     * @return session
     */
    protected StatelessKieSession newSession() {
        StatelessKieSession session = kieBase().newStatelessKieSession();
        session.addEventListener(eventListenerLogger);

        return session;
    }

    /**
     * Appropriate KieBase for particular flow
     * @return kiebase
     */
    protected abstract KieBase kieBase();

}
