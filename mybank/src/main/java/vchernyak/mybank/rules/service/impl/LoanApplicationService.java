package vchernyak.mybank.rules.service.impl;

import org.kie.api.KieBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.rules.data.input.impl.LoanApplicationInput;
import vchernyak.mybank.rules.log.EventListenerLogger;
import vchernyak.mybank.rules.service.RulesService;

/**
 * Processes rules of loan application and returns result.
 * For current implementation resulting object is Loan with interest set up
 */
@Service
public class LoanApplicationService extends RulesService<Loan, LoanApplicationInput> {

    private final KieBase kieBase;

    @Autowired
    public LoanApplicationService(EventListenerLogger eventListenerLogger, LoanRepository loanRepository, @Qualifier("loanApplicationKieBase") KieBase kieBase) {
        super(eventListenerLogger, loanRepository);
        this.kieBase = kieBase;
    }

    /**
     * kiebase of loan application flow
     * @return kiebase
     */
    @Override
    protected KieBase kieBase() {
        return kieBase;
    }
}
