package vchernyak.mybank.rules.service.impl;

import org.kie.api.KieBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.rules.data.input.impl.LoanExtensionInput;
import vchernyak.mybank.rules.log.EventListenerLogger;
import vchernyak.mybank.rules.service.RulesService;

/**
 * Processes rules of loan application and returns result.
 * For current implementation resulting object is new Loan with interest and end increased
 */
@Service
public class LoanExtensionService extends RulesService<Loan, LoanExtensionInput> {

    private final KieBase kieBase;

    @Autowired
    public LoanExtensionService(EventListenerLogger eventListenerLogger, LoanRepository loanRepository, @Qualifier("loanExtensionKieBase") KieBase kieBase) {
        super(eventListenerLogger, loanRepository);
        this.kieBase = kieBase;
    }

    @Override
    protected KieBase kieBase() {
        return kieBase;
    }
}
