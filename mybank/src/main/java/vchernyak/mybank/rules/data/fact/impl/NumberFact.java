package vchernyak.mybank.rules.data.fact.impl;

import vchernyak.mybank.rules.data.fact.NamedFact;

/**
 * {@link vchernyak.mybank.rules.data.fact.NamedFact} implementation for {@link java.lang.Number}
 */
public class NumberFact extends NamedFact {

    private final Number value;

    public NumberFact(String name, Number value) {
        super(name);
        this.value = value;
    }

    public Number getValue() {
        return value;
    }
}
