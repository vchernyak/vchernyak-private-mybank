package vchernyak.mybank.rules.data.fact.impl;

import vchernyak.mybank.rules.data.fact.NamedFact;

/**
 * {@link vchernyak.mybank.rules.data.fact.NamedFact} implementation for {@link java.lang.Boolean}
 */
public class BooleanFact extends NamedFact {

    private final Boolean value;

    public BooleanFact(String name, Boolean value) {
        super(name);
        this.value = value;
    }

    public Boolean getValue() {
        return value;
    }
}
