package vchernyak.mybank.rules.data.input.impl;

import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.rules.data.fact.NamedFact;
import vchernyak.mybank.rules.data.fact.impl.*;
import vchernyak.mybank.rules.data.input.RuleInput;

import java.util.Arrays;
import java.util.List;

/**
 * Input for loan extension
 */
public class LoanExtensionInput implements RuleInput<Loan> {

    private static final String QUERY_LOAN = "loan";

    private final Loan loan;

    private final String ip;

    public LoanExtensionInput(Loan loan, String ip) {
        this.loan = loan;
        this.ip = ip;
    }

    @Override
    public List<NamedFact> facts() {
        return Arrays.asList(
                new LoanFact("loan", loan),
                new StringFact("ip", ip)
        );
    }

    @Override
    public String resultQuery() {
        return QUERY_LOAN;
    }
}
