package vchernyak.mybank.rules.data.result;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

/**
 * Result or rules execution
 */
public class RuleResult<T> {

    private final boolean success;

    private final List<String> messages;

    private final T result;

    public RuleResult(boolean success, List<String> messages, T result) throws IllegalArgumentException {
        if(messages == null) {
            throw new IllegalArgumentException("Messages may not be null");
        }

        this.success = success;
        this.messages = messages;
        this.result = result;
    }

    /**
     * Is execution result was successfull
     * @return true if success false otherwise
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Execution messages
     * @return list of execution messages, not null
     */
    public List<String> getMessages() {
        return messages;
    }

    /**
     * Resulting object
     * @return generic resulting object, may be null
     */
    public T getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
