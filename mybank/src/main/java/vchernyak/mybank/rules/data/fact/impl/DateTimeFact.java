package vchernyak.mybank.rules.data.fact.impl;

import org.joda.time.DateTime;
import vchernyak.mybank.rules.data.fact.NamedFact;

/**
 * {@link vchernyak.mybank.rules.data.fact.NamedFact} implementation for {@link org.joda.time.DateTime}
 */
public class DateTimeFact extends NamedFact {

    private final DateTime value;

    public DateTimeFact(String name, DateTime value) {
        super(name);
        this.value = value;
    }

    public DateTime getValue() {
        return value;
    }
}
