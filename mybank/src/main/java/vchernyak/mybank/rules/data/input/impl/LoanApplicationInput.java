package vchernyak.mybank.rules.data.input.impl;

import org.joda.time.DateTime;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.rules.data.fact.*;
import vchernyak.mybank.rules.data.fact.impl.*;
import vchernyak.mybank.rules.data.input.RuleInput;

import java.util.Arrays;
import java.util.List;

/**
 * Input for loan application
 */
public class LoanApplicationInput implements RuleInput<Loan> {

    private static final String QUERY_LOAN = "loan";

    private final User user;

    private final Loan loan;

    private final Double amount;

    private final DateTime applicationTime;

    private final String ip;

    public LoanApplicationInput(User user, Loan loan, Double amount, DateTime applicationTime, String ip) {
        this.user = user;
        this.loan = loan;
        this.amount = amount;
        this.applicationTime = applicationTime;
        this.ip = ip;
    }

    @Override
    public List<NamedFact> facts() {
        return Arrays.asList(
                new UserFact("user", user),
                new LoanFact("loan", loan),
                new StringFact("ip", ip),
                new NumberFact("amount", amount),
                new DateTimeFact("application time", applicationTime)
        );
    }

    @Override
    public String resultQuery() {
        return QUERY_LOAN;
    }
}
