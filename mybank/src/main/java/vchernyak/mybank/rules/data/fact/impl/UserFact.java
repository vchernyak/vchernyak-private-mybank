package vchernyak.mybank.rules.data.fact.impl;

import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.rules.data.fact.NamedFact;

/**
 * {@link vchernyak.mybank.rules.data.fact.NamedFact} implementation for {@link vchernyak.mybank.dao.entity.User}
 */
public class UserFact extends NamedFact {

    private final User value;

    public UserFact(String name, User value) {
        super(name);
        this.value = value;
    }

    public User getValue() {
        return value;
    }
}
