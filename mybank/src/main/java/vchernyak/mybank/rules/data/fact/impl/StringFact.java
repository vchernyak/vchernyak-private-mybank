package vchernyak.mybank.rules.data.fact.impl;

import vchernyak.mybank.rules.data.fact.NamedFact;

/**
 * {@link vchernyak.mybank.rules.data.fact.NamedFact} implementation for {@link java.lang.String}
 */
public class StringFact extends NamedFact {

    private final String value;

    public StringFact(String name, String value) {
        super(name);
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
