package vchernyak.mybank.rules.data.fact.impl;

import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.rules.data.fact.NamedFact;

/**
 * {@link vchernyak.mybank.rules.data.fact.NamedFact} implementation for {@link vchernyak.mybank.dao.entity.Loan}
 */
public class LoanFact extends NamedFact {

    private final Loan value;

    public LoanFact(String name, Loan value) {
        super(name);
        this.value = value;
    }

    public Loan getValue() {
        return value;
    }
}
