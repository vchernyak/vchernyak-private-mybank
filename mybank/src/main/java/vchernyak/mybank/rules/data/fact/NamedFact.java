package vchernyak.mybank.rules.data.fact;

/**
 * Base class for named usertype facts to be inserted in drools working memory
 * No abstract method getValue() or so stated, because of drool's unawareness about real result type in this case.
 * No generics used, because generics matters only for compiler.
 */
public abstract class NamedFact {

    private final String name;

    protected NamedFact(String name) {
        this.name = name;
    }

    /**
     * @return name of the fact
     */
    public final String getName() {
        return name;
    }

}
