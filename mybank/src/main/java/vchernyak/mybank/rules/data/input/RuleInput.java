package vchernyak.mybank.rules.data.input;

import vchernyak.mybank.rules.data.fact.NamedFact;

import java.util.List;

/**
 * Interface of basic API for running drools logic with {@link vchernyak.mybank.rules.service.RulesService}
 */
public interface RuleInput<T> {

    /**
     * List of named facts to be placed in drools working memory to run logic on
     */
    List<NamedFact> facts();

    /**
     * Name of query that provides result of execution
     */
    String resultQuery();
}
