package vchernyak.mybank.web.service;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import vchernyak.mybank.dao.LoanRepository;
import vchernyak.mybank.dao.UserRepository;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.dao.entity.User;
import vchernyak.mybank.json.web.LoanApplication;
import vchernyak.mybank.rules.data.input.impl.LoanApplicationInput;
import vchernyak.mybank.rules.data.input.impl.LoanExtensionInput;
import vchernyak.mybank.rules.data.result.RuleResult;
import vchernyak.mybank.rules.service.impl.LoanApplicationService;
import vchernyak.mybank.rules.service.impl.LoanExtensionService;
import vchernyak.mybank.security.auth.TokenAuthentication;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Service with {@link vchernyak.mybank.web.controller.impl.LoansController} logic.<br />
 */
@Service
public class LoansControllerService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private LoanApplicationService loanApplicationService;

    @Autowired
    private LoanExtensionService loanExtensionService;

    /**
     * All user's loans
     */
    public List<Loan> retrieveLoans(TokenAuthentication auth) {
        return loanRepository.findByUserOrderByStartAsc(userByAuthentication(auth));
    }

    /**
     * User loan by id
     */
    public ResponseEntity<Loan> retrieveLoanById(final long id, TokenAuthentication auth) {
        Loan loan = loanRepository.findByUserAndId(userByAuthentication(auth), id);
        if(loan != null) {
            return new ResponseEntity<>(loan, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * User loan by id with history
     */
    public ResponseEntity<List<Loan>> retrieveLoanByIdWithHistory(final long id, TokenAuthentication auth) {
        Loan loan = loanRepository.findByUserAndId(userByAuthentication(auth), id);
        if(loan == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<Loan> loansHistory = new ArrayList<>();
        Loan currentLoan = findTopParentLoan(loan);
        do {
            loansHistory.add(currentLoan);
        } while ((currentLoan = currentLoan.getExtension()) != null);

        return new ResponseEntity<>(loansHistory, HttpStatus.OK);
    }

    /**
     * Loan application
     */
    public ResponseEntity applyLoan(final LoanApplication loanApplication, TokenAuthentication auth) throws URISyntaxException {
        User user = userByAuthentication(auth);

        Loan loan = new Loan.Builder()
                .withStart(DateTime.now(DateTimeZone.UTC))
                .withEnd(loanApplication.getEnd())
                .withAmount(loanApplication.getAmount())
                .withUser(user)
                .withUserActionInfo(auth.getIp())
                .build();

        LoanApplicationInput input = new LoanApplicationInput(user, loan, loan.getAmount(), loan.getStart(), auth.getIp());
        RuleResult<Loan> result = loanApplicationService.process(input);

        if (result.isSuccess()) {
            loan = result.getResult();

            URI location = new URI("/loans/" + loan.getId());

            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setLocation(location);

            return new ResponseEntity(responseHeaders, HttpStatus.CREATED);
        } else {
            return new ResponseEntity(result.getMessages(), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Loan extension
     */
    public ResponseEntity extendLoan(final long id, TokenAuthentication auth) throws URISyntaxException {
        User user = userByAuthentication(auth);

        Loan loanToExtend = loanRepository.findByUserAndId(user, id);
        if(loanToExtend == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        LoanExtensionInput input = new LoanExtensionInput(loanToExtend, auth.getIp() );
        RuleResult<Loan> result = loanExtensionService.process(input);

        if(result.isSuccess()) {
            Loan extended = result.getResult();

            URI location = new URI("/loans/" + extended.getId());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.setLocation(location);

            return new ResponseEntity(responseHeaders, HttpStatus.CREATED);
        } else {
            return new ResponseEntity(result.getMessages(), HttpStatus.BAD_REQUEST);
        }
    }

    private User userByAuthentication(TokenAuthentication auth) {
        return userRepository.findByUsername(auth.getPrincipal());
    }

    private Loan findTopParentLoan(final Loan loan) {
        if(loan.getExtended() != null) {
            return findTopParentLoan(loan.getExtended());
        }
        return loan;
    }
}
