package vchernyak.mybank.web.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.async.DeferredResult;
import vchernyak.mybank.security.auth.TokenAuthentication;
import vchernyak.mybank.web.async.RequestTask;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Base class for controllers.<br />
 * Supports queuing of user operations for asynchronous result.
 */
public abstract class BaseController {

    private final Map<String, ThreadPoolExecutor> userQueuedExecutor = new HashMap<>();

    /**
     * Current user's authentication
     */
    protected TokenAuthentication getAuthentication() {
        return (TokenAuthentication) SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * Current user's username
     */
    protected String getUsername() {
        return getAuthentication().getPrincipal();
    }

    /**
     * Submits task for execution <br />
     * Execution is made as user queue - one user's operation at a time.<br />
     * Separate user's operations are concurrent
     * @param task {@link RequestTask} implemetation
     * @return deferred result
     */
    protected DeferredResult<ResponseEntity> submitToExecutor(RequestTask task) {
        synchronized (userQueuedExecutor) {
            ThreadPoolExecutor executor = userQueuedExecutor.get(getUsername());
            if(executor == null) {
                executor = ((ThreadPoolExecutor) Executors.newFixedThreadPool(1));
                userQueuedExecutor.put(getUsername(), executor);
            }

            executor.submit(task);
        }
        return task.getDeferredResult();
    }

    /**
     * Scheduled cleanup of inactive user queues.
     */
    @Scheduled(fixedDelay = 10000)
    public void clearUnusedExecutors() {
        synchronized (userQueuedExecutor) {
            for (Iterator<Map.Entry<String, ThreadPoolExecutor>> iterator = userQueuedExecutor.entrySet().iterator(); iterator.hasNext(); ) {
                Map.Entry<String, ThreadPoolExecutor> entry = iterator.next();
                ThreadPoolExecutor executor = entry.getValue();
                if (executor.getActiveCount() == 0 && executor.getQueue().size() == 0) {
                    iterator.remove();
                    executor.shutdown();
                }
            }
        }
    }

}
