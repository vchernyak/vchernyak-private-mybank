package vchernyak.mybank.web.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import vchernyak.mybank.dao.entity.Loan;
import vchernyak.mybank.json.web.LoanApplication;
import vchernyak.mybank.security.auth.TokenAuthentication;
import vchernyak.mybank.web.controller.BaseController;
import vchernyak.mybank.web.async.RequestTask;
import vchernyak.mybank.web.service.LoansControllerService;

import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for loan operations
 */
@RestController
@RequestMapping("/loans")
public class LoansController extends BaseController {

    @Autowired
    private LoansControllerService loansControllerService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Loan> retrieveLoans() {
        return loansControllerService.retrieveLoans(getAuthentication());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Loan> retrieveLoanById(@PathVariable("id") final long id) {
        return loansControllerService.retrieveLoanById(id, getAuthentication());
    }

    @RequestMapping(value = "/{id}/history", method = RequestMethod.GET)
    public ResponseEntity<List<Loan>> retrieveLoanByIdWithHistory(@PathVariable("id") final long id) {
        return loansControllerService.retrieveLoanByIdWithHistory(id, getAuthentication());
    }

    @RequestMapping(method = RequestMethod.POST)
    public DeferredResult<ResponseEntity> applyLoan(@RequestBody final LoanApplication loanApplication) throws URISyntaxException {
        final TokenAuthentication auth = getAuthentication();
        return submitToExecutor(new RequestTask() {
            @Override
            protected ResponseEntity doTask() throws Exception {
                return loansControllerService.applyLoan(loanApplication, auth);
            }
        });
    }

    @RequestMapping(value = "/{id}/extend", method = RequestMethod.POST)
    public DeferredResult<ResponseEntity> extendLoan(@PathVariable("id") final long id) throws URISyntaxException {
        final TokenAuthentication auth = getAuthentication();
        return submitToExecutor(new RequestTask() {
            @Override
            protected ResponseEntity doTask() throws Exception {
                return loansControllerService.extendLoan(id, auth);
            }
        });
    }


}
