package vchernyak.mybank.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

/**
 * Controller exceptions handler
 */
@ControllerAdvice
public class ExceptionHandlerController {

    private final Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class);

    /**
     * Handles uncaught exceptions on controllers. Sets status 400, writes details to log with INFO level.
     */
    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e) {
        log.info("REQUEST HANDLING EXCEPTION DETAILS : {}", errorInfo(request, e));
        return null;
    }

    private String errorInfo(HttpServletRequest request, Exception e) {
        Map<String, String> info = new LinkedHashMap<>();
        info.put("METHOD", request.getMethod());
        info.put("URI", request.getRequestURL().toString());
        if(SecurityContextHolder.getContext().getAuthentication() != null) {
            info.put("AUTHENTICATION", SecurityContextHolder.getContext().getAuthentication().toString());
        }
        info.put("HEADERS", headers(request));
        info.put("EXCEPTION", exceptionTrace(e));

        StringBuilder sb = new StringBuilder("{\n");
        for (String key : info.keySet()) {
            sb.append(key).append("\t:\t").append(info.get(key)).append("\n");
        }
        sb.append("\n}");

        return sb.toString();
    }

    private String exceptionTrace(Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    private String headers(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();

        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            sb.append("\n").append(headerName.toUpperCase()).append(":\t");

            Enumeration<String> headers = request.getHeaders(headerName);
            while (headers.hasMoreElements()) {
                String header = headers.nextElement();
                sb.append(header).append(";");
            }

        }

        return sb.toString();
    }

}
