package vchernyak.mybank.web.async;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * Implement this (often like anonymous class) to return asynchronous result from controller
 */
public abstract class RequestTask implements Runnable {

    private final DeferredResult<ResponseEntity> deferredResult;

    public RequestTask() {
        this.deferredResult = new DeferredResult<>();
    }

    @Override
    public void run() {
        try {
            deferredResult.setResult(doTask());
        } catch (Exception e) {
            deferredResult.setResult(new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * Here place logic that should be asynchronous
     * @return controller method result
     * @throws Exception exception may be throwed, will be handled in caller method as status 500
     */
    protected abstract ResponseEntity doTask() throws Exception;

    /**
     * Result returned from to asynchronous controller method
     */
    public DeferredResult<ResponseEntity> getDeferredResult() {
        return deferredResult;
    }
}
