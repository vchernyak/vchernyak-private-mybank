package vchernyak.mybank;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Main Spring configuration.
 * Entry point to other configurations.
 * Used in starter class to launch application.
 * Also used in happy path tests.
 *
 * Other configuration-annotated classes are:<br/>
 *      {@link vchernyak.mybank.web.WebConfig} <br/>
 *      {@link vchernyak.mybank.security.SecurityConfig}<br/>
 *      {@link vchernyak.mybank.rules.RulesConfig}<br/>
 *      {@link vchernyak.mybank.json.JacksonConfig}<br/>
 */
@ComponentScan("vchernyak.mybank.*")
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class})
@EnableScheduling
public class ApplicationConfig {}
