package rules.loanApplication;
import vchernyak.mybank.rules.data.fact.impl.BooleanFact;
import vchernyak.mybank.rules.data.fact.impl.NumberFact;

global vchernyak.mybank.dao.LoanRepository loanRepository;

dialect  "mvel"

/**
 * Loan application decision.
 * Salience up to 9
 */

//Loan refused
rule "Loan Application: refuse loan" salience (0)
when
    not BooleanFact(name == "success")
    (
        not BooleanFact(name == "amount valid", value == true)
        or
        not BooleanFact(name == "time valid", value == true)
        or
        not BooleanFact(name == "count valid", value == true)
    )
then
    insert(new BooleanFact("success", false))
end

//Loan applied and saved
rule "Loan Application: apply loan" salience (0)
when
    exists BooleanFact(name == "amount valid", value == true)
    exists BooleanFact(name == "time valid", value == true)
    exists BooleanFact(name == "count valid", value == true)
    NumberFact(name == "interest", $interest : value.doubleValue())
    Input($loan : loan)
then
    $loan.setInterest($interest);

    loanRepository.save($loan);

    insert(new BooleanFact("success", true))
end